<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title> Admin </title>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/font-aw/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="css/AdminDashboard.css">
    <!-- Google Font -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="login-page">
    <header class="bg-light-header">
        <div class="container">
            <div class="row">
                <div class=" col-8">
                    <nav class="navbar navbar-expand-lg navbar-dark indigo">
                        <a class="navbar-brand" href="#">happy travels</a>
                        </nav>
                </div>
                        <div class=" col-4">
                            <ul class="navLink nav" role="list">
                                <li role="listitem" class="nav-item">
                                    <a href="/logout" class="nav-link log_out">
                                        <i class="fa fa-power-off fa-xs" aria-hidden="true"></i>
                                        Log Out
                                    </a>
                                </li>
                            </ul>
                        </div>
                </div>
            </div>
    </header>
    <div class="container">
        <div class="row ">
            <div class="col-lg-12 mt-3">
                <div class="card">
                    <div class="card-header">
                        Agents  
                    </div>
                    <div class="card-body">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">Agent #</th>
                                    <th scope="col">Agent Name</th>
                                    <th scope="col">Agent Email</th>
                                    <th scope="col">Agent Comm File</th>
                                    <th scope="col">Agent Status</th>
                                    <th scope="col">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($agents as $agent)
                                    <tr>
                                        <th scope="row">{{$agent->id}}</th>
                                        <td>{{$agent->name}}</td>
                                        <td>{{$agent->email}}</td>
                                        <td><a href="/download-comm-file/{{$agent->id}}" target="_blank">View File</a></td>
                                        <td>
                                            @if($agent->status == 1)
                                                Approved
                                            @else
                                            Pending 
                                            @endif
                                        </td>
                                        <td>
                                            <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
                                                @if($agent->status == 1)
                                                    <a href="/decline/{{$agent->id}}" class="btn btn-danger ">Pending</a>
                                                @else
                                                    <a href="/approve/{{$agent->id}}" class="btn btn-success">Approve</a>
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
