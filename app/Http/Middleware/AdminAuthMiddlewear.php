<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;

class AdminAuthMiddlewear
{
    public function handle($request, Closure $next){
        if (!auth()->check()) {
            return redirect(action('Auth\LoginController@viewLogin'));
        }
        return $next($request);
    }
}
