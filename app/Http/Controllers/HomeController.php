<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\User;
use Mail;
use Redirect;
class HomeController extends Controller
{
    public function viewHome(){
        $agents = User::getAgents();
        return view('dashbored',['agents' => $agents]);
    }

    public function downloadCommFile($id){
        // $destinationPath = "C:\\xampp\htdocs\agents\documents/".$id;
        $destinationPath = "/var/www/html/haneen/agents/documents/" . $id;

        $kycZipFile = '/tmp/'.$id . '_comm_file.zip';

         //Get real path for our folder
        $rootPath = realpath($destinationPath);

        // Initialize archive object
        $zip = new \ZipArchive();
        $zip->open($kycZipFile, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

        $files = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator($rootPath),
            \RecursiveIteratorIterator::LEAVES_ONLY);

        foreach ($files as $name => $file){
            // Skip directories (they would be added automatically)
            if (!$file->isDir()){
               //  Get real and relative path for current file
                $filePath = $file->getRealPath();
                $relativePath = substr($filePath, strlen($rootPath) + 1);
                $zip->addFile($filePath, $relativePath);
            }
        }
        // Zip archive will be created only after closing object
         $zip->close();
        // download the file
         header('Content-Description: File Transfer');
         header('Content-Type: application/octet-stream');
         header('Content-Disposition: attachment; filename='.basename($kycZipFile));
         header('Content-Transfer-Encoding: binary');
         header('Expires: 0');
         header('Cache-Control: must-revalidate');
         header('Pragma: public');
         header('Content-Length: ' . filesize($kycZipFile));
         readfile($kycZipFile);

         // remove the created zip file
         unlink($kycZipFile);
     }

    public function approve($agentId){
        // update agent record status to 1
        User::updateAgentStatus($agentId,'1');
        // $data = array('name'=>"Virat Gandhi");

        // // Mail::send(['text' => 'mailme'], $data, function($message) {
        // //     $message->to('abc@gmail.com', 'Tutorials Point')->subject
        //         ('Laravel Basic Testing Mail');
        //     $message->from('xyz@gmail.com','Virat Gandhi');
        // });
        // echo "Basic Email Sent. Check your inbox.";

        return Redirect::to(action('HomeController@viewHome'));

    }

    public function decline($agentId){
        // update agent record status to 0
        User::updateAgentStatus($agentId,'0');

        return Redirect::to(action('HomeController@viewHome'));
    }
}
