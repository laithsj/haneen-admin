<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',['uses' => 'HomeController@viewHome','middleware' => ['adminAuth']]);


// login
Route::get('/login',['uses' => 'Auth\LoginController@viewLogin']);
Route::post('/login',['uses' => 'Auth\LoginController@login']);

// logout
Route::get('/logout',['uses' => 'Auth\LoginController@logout']);

// home route
Route::get('/home',['uses' => 'HomeController@viewHome','middleware' => ['adminAuth']]);

// download comm file
Route::get('/download-comm-file/{id}',['uses' => 'HomeController@downloadCommFile']);


// approve and decline routes
Route::get('/approve/{id}',['uses' => 'HomeController@approve']);
Route::get('/decline/{id}',['uses' => 'HomeController@decline']);
